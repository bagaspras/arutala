# QA Tester Intern ARUTALA
1. What do you know about Test case? Please explain.
2. What do you know about Application Programming Interface (API)? Please explain.
3. What do you know about Acceptance Testing? Please explain.
4. What do you do when your bugs won’t be fixed by developers but critical bugs?
Please explain.

5. Create Test Case and Automation for website http://automationpractice.com.
Upload to your Gitlab then explain on ReadMe about the framework you are using
and how to run your automation. (No minimum test case, create as many as you can)
Jawab : Saya menggunakan selenium dalam automation testing, selenium yang saya gunakan yaitu menggunakan lib Selenium pada Python.
Maka cara run automationnya adalah memerintahkan selenium membuka browser dengan webdriver lalu menuju pada website yang ingin ditest. Namun ada beberapa kendala sehingga automationnya tidak berjalan dengan baik, yaitu terjadi error pada syntax dimana saya menggunakan IDE PyCharm berbasis Windows sehingga minim dalam menemukan solusi baik di stackoverflow maupun di forum GitHub.

6. Create Test case and Automation for mobile apps (use this apk, download here).
Upload to your Gitlab then explain on ReadMe about the framework you are using
and how to run your automation. (No minimum test case , create as many as you
can)
Jawab : Selendroid merupakan framework tool yang cukup baik dalam testing apk android. Namun sama seperti selenium, kendala yang saya hadapi mirip sehingga menyebabkan terjadinya error. 

7. Create Test case and Automation for API(use this api). Upload to your Gitlab then
explain on ReadMe about the framework you are using and how to run your
automation. (No minimum test case , create as many as you can)
Jawab : Automation menggunakan postman, namun kendalanya adalah saya belum familiar dengan testing API sehingga saya masih kesulitan dalam melakukan automation.
